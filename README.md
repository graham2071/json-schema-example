Tests json schema implementation of polymorphism-like modelization.

Data requirement: data shall contain a list of animals. An animal may be: a dog, a wolf, or a turtle. Each animal share common properties (name), and specific properties (owner for dogs, pack for wolves and speed for turtles).

Implementation requirements:
  - no code duplication (common properties are written only once)
  - the json schema validation errors shall be as clear as possible for the user.

The validator used in the is Ajv.

For testing purposes, the data file (animals.json) contains a list with two animals: a dog (no error) and a wolf that contains a validation error ('pack' is defined as an integer instead of a string).


Case 1
------
An AbtractAnimal has a type property. Each animal implentation "forces" this type property. An Animal is a kind of switch ('anyOf') on any animal.
Result: ajv raises 6 errors, because it explores each possible branch.

Case 2
------
Same as Case 1, but with an "Animal Container" class.
Result: idem Case 1

Case 3
------
The "type" property object is removed, and each animal has now a specific property.
Result: ajv validates with 4 errors. Each animal branch is still explored (but raises only one error per branch, plus one for the 'anyOf').

Case 4
------
Same as Case 1 (type property), but now the animal switch is done with a "if".
Result: ajv validates with two errors. One for the expected error, one error for the unresolved then of the wolf branch. This number of errors is independant on the number of animal types defined in the schema.

Case 5
------
Same as Case 2, but with "if" as in "Case 3".

Usage
=====
Simply browse files, or run the tests with:
```
npm install
npm test
```

Note that the tests volontarily fail.

You can also restrict the tests to a regular expression. For example:
```
npm test -- -g case1
```
