import * as ajv from 'ajv';
import * as glob from 'glob';
import * as fs from 'fs';
import { expect } from 'chai';
import 'mocha';


export function testValidation(schemaFile: string, dataFile: string) {

    let validatorFactory: ajv.Ajv = ajv({ allErrors: true });

    describe('Validate JSON files with json schema', () => {
        it(dataFile + ' should validate', () => {
            let validate = validatorFactory.compile(require(schemaFile));
            const data = JSON.parse(fs.readFileSync(dataFile, 'utf8'));
            let validationResult = validate(data) as boolean;
            let errorsNb = 0;

            if (validationResult) {
                console.log("Validation of ", dataFile, " [SUCCESS]");
            } else {
                errorsNb = validate.errors.length;
                console.log("Validation of ", dataFile, " [FAILURE]");
                console.log(validate.errors);
            }
            expect(errorsNb).to.equal(1);
        });
    });
}
